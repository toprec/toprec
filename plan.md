# Topological Recursion programming project

## Who

- ...

## Existing projects

* [qairy](https://github.com/gkpotter/qairy) by [Greyson Potter](https://gkpotter.com/)
* naive ABCD implementation in [surface-dynamics](https://flatsurf.github.io/surface-dynamics/topological_recursion/index.html) by Vincent Delecroix
* Python code by Bertrand Eynard on gitlab
* Vincent Bouchard Mathematica code 

## Tools

- host on remote git server (eg gitlab, github or [plmlab](https://plmlab.math.cnrs.fr)
  
- implement in python, sagemath
- maybe in mathematica ?

## Project

Easy to share/use library.

The program should work for various types of spectral curves. In applications, spectral curves can be one (or several of):
- parametrized spectral curves
    - explicit rational parametrization such as $x(z) = z^2$, $y(z) = z$ (rather a one form $y dx$), $B(z_1,z_2) = \frac{dz_1\ dz_2}{(z_1 - z_2)^2}$
    - explicit genus one parametrization (like Weierstrass or theta)
    - explicit higher genus one parametrization
- implicit spectral curve, ie a polynomial equation of a plane curve $P(x,y) = 0$ (or projective) possibly with parameters such as $P(x,y) = y^2 - x^3 - g_2 x - g_3$ possibly not algebraic such as $P(x,y) = y^2 - sin(\sqrt{x})^2$
- a set of KP times $t_k$ (= Taylor expansion of $y$ at $\infty$)
- a set of KdV times $t_k$ (= Taylor expansion of $y$ at $0$)
- some ABCD tensors, i.e. quantum Airy structure
  (for simple branch points)
- have 1 branch point
- have several branchpoints
- have higher order branchpoints (see Bouchard-Eynard local/global)
...

---

# UML graphs
```mermaid
classDiagram
     class RiemannSurface

     class SpectralCurve{
         C : RiemannSurface
         x : MeromorphicFunction
         y : OneForm
         B : BiDifferential
     }
```

## Issues, questions

### fast tensor computations

See [arXiv:1703.03307](https://arxiv.org/abs/1703.03307). As a result of TR we obtain tensors $F_{g,n}[i_0, i_1, \ldots, i_{n-1}]$ and perform operations such as
$$
\sum_{a,b} C[i_0,a,b] F_{1,5}[a,b,i_1,i_2,i_3]
$$
or
$$
\sum_{a,b} C[i_0,a,b] F_{1,3}[a,i_2,i_3] F_{1,2}[b,i_1]
$$
that must be fast. Here $C$ is an infinite tensor and the $F$ are finite.

Notes
- the $F_{g,n}$ tensors are symmetric!
- when there are more ramification points, the indices become "(ramification point, integer index)"
- need to carefully sum over meaningful indices in contractions (eg for Kontsevich the only non-zero coefficients of $F_{g,n}$ are the ones for which $i_0+i_1+\ldots+i_{n-1}=3g-3+n$)
- the result must be a tensor with indices in the correct order: _re-permute_ . 
exemple: the result of $\sum_{a,b} C[i_0,a,b] F_{1,3}[a,i_2,i_3] F_{1,2}[b,i_1]$ should contribute to $F_{5,2}[i_0,i_1,i_2,i_3]$ and not $F_{5,2}[i_0,i_2,i_3,i_1]$

Existing implementations
- [eigen](https://eigen.tuxfamily.org/) and especially the [eigen-unsupported/tensors](https://eigen.tuxfamily.org/dox/unsupported/eigen_tensors.html)

### Computing B (for plane curves)

For an algebraic plane curve, $B$ is _nearly_ cannonical.
In fact for an algebraic plane curve with a Torelli marking (choide of A and B cycles) it is cannonical.

**Problem:** implement it.

- There exists a general expression of $B$ in terms of $\Theta$-functions. Problem: $\Theta$-functions are transcendantal functions whereas $B$ should be an algebraic function, so this is far from optimal.


- An algorithm for finding a rational expression of $B$ was developed by Baker,  by  Weierstrass or by Klein. However it is an algorithm and not a formula. Is it implemented anywhere ?

- in https://arxiv.org/abs/1805.07247, there is an exact and rather compact formula. It is very well adapted to implementation, it uses only combinatorics in the Newton's polygon.
 
**Goal:** implement https://arxiv.org/abs/1805.07247
This could be a project rather independent from TR.
