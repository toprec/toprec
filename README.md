# Topological Recursion

<img src="Ressources/top_rec.png" width="100%">

<!-- ![image](Ressources/top_rec.png) -->

Acronym: **toprec**

see project:
<a href="plan.md">plan.md</a>

##### What is Topological Recursion:
  wikipedia: https://en.wikipedia.org/wikiTopological_recursion

  nlab: https://ncatlab.org/nlab/show/topological+recursion 




##### Switch branch:
Development is taking place in branch `dev`.





---

## Description

Implementation of topological recursion

---

## Usage

to be written




---
## Contributing

...

---
## Authors and acknowledgment

B. Eynard
D. Mitsios
V. Delecroix
J. Schmitt

**Aknowledgements:**
- ERC ReNewQuantum

---
## License

For open source projects, say how it is licensed.

---
## Project status

In development
started june 2023
